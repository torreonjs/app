import {
  GET_CATALOG,
} from '../actions/types/CatalogTypes';
import * as data from '../../helpers/data/catalog.json';

const INITIAL_STATE = {
  catalogList: data
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_CATALOG:
      return {
        ...state,
        catalogList: []
      };
    default:
      return state;
  }
}
