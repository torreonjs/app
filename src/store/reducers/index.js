import { combineReducers } from 'redux'
import CatalogReducer from './CatalogReducer'
import CartReducer from './CartReducer'
import ProductReducer from './ProductReducer'

export default combineReducers({
  catalog: CatalogReducer,
  cart: CartReducer,
  product: ProductReducer
})
