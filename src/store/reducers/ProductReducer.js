import {
  RESET_PRODUCT,
  SET_PRODUCT
} from '../actions/types/ProductTypes';
import * as data from '../../helpers/data/product.json';

/*const INITIAL_STATE = {
  id: null,
  name: '',
  description: ''
};*/

const INITIAL_STATE = data;

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case RESET_PRODUCT:
      return INITIAL_STATE;
    case SET_PRODUCT:
      return {
        ...state,
        id: action.payload.id,
        name: action.payload.name,
        description: action.payload.description
      };
    default:
      return state;
  }
}
