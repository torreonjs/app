import {
  ADD_PRODUCT_CART
} from '../actions/types/CartTypes';
import * as data from '../../helpers/data/catalog.json';

const INITIAL_STATE = {
  price: 0,
  quantity: 0,
  products: data
  //products: []
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ADD_PRODUCT_CART:
      var pair = {key: action.payload};
      return {
        ...state,
        products: [
          ...state.products,
          ...pair
        ]
      };
    default:
      return state;
  }
}
