import {
  SET_USER_PROFILE
} from '../actions/types/AuthTypes'

const INITIAL_STATE = {
  user: {}
}

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_USER_PROFILE:
      return {
        ...state,
        user: action.payload
      }
  }
}
