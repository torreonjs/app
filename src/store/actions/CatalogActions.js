import {
  GET_CATALOG
} from './types/GroupsTypes';

export const setInitialGroups = groupList => {
  return {
    type: GET_CATALOG,
    payload: groupList
  };
};
