import {
  SET_PRODUCT
} from './types/ProductTypes';

export const setProduct = product => {
  console.log("setProduct");
  console.log(product);
  return {
    type: SET_PRODUCT,
    payload: product
  };
};
