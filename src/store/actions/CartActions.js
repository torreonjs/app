import {
  ADD_PRODUCT_CART,
  RESET_PRODUCT
} from './types/CartTypes';

export const addProductToCart = product => {
  return {
    type: ADD_PRODUCT_CART,
    payload: product
  };
};

export const resetProduct = () => {
  return {
    type: RESET_PRODUCT
  };
};

