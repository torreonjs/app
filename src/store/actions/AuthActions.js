import {
  SET_USER_PROFILE
} from './types/AuthTypes'

export const setUserProfile = user => {
  return {
    type: SET_USER_PROFILE,
    payload: user
  }
}
