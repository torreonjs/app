
import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View
} from 'react-native'
import firebase from 'react-native-firebase'
import { Actions } from 'react-native-router-flux'

const Landing = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Landing
      </Text>
      <Text
        style={styles.welcome}
        onPress={() => Actions.qrscanner()}
      >
        Go to QRScanner
      </Text>
      <Text
        style={styles.welcome}
        onPress={() => Actions.product()}
      >
        Go to Product
      </Text>
      <Text
        style={styles.welcome}
        onPress={async () => {
          await firebase.auth().signOut()
          Actions.login()
        }}
      >
        Logout
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

export default Landing;