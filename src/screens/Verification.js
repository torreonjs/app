
import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import logo from '../img/logo.png'

class Verification extends Component {
  constructor (props) {
    super(props)

    this.state = {
      code: ''
    }

    this.validateCode = this.validateCode.bind(this)
  }

  async validateCode () {
    try {
      const user = await this.props.confirmation.confirm(this.state.code)
      console.log('create token here', user)
      Actions.reset('main')
    } catch (e) {
      console.error(e)
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <Image
          source={logo}
          resizeMode='contain'
          cache='force-cache'
          style={{ width: 300 }}
        />
        <Text style={styles.welcome}>
          Ingresa tu código
        </Text>
        <TextInput
          underlineColorAndroid={'transparent'}
          autoCorrect={false}
          selectionColor={'#fff'}
          placeholder={'Código'}
          keyboardType='phone-pad'
          maxLength={6}
          placeholderTextColor={'#999999'}
          onChangeText={(text) => { this.setState({ code: text }) }}
          value={this.state.code}
          returnKeyType={'next'}
          onSubmitEditing={this.validateCode}
          blurOnSubmit={false}
          style={[styles.fullInput, { width: 300 }]}
        />
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.validateCode}
          style={styles.actionButton}
        >
          <Text style={{ color: '#fff', fontSize: 20 }}>Continuar</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffdc20'
  },
  welcome: {
    fontSize: 24,
    textAlign: 'center',
    margin: 10,
    color: '#e81f25'
  },
  fullInput: {
    width: 300,
    backgroundColor: '#fff',
    padding: 15,
    borderRadius: 5,
    fontSize: 16
  },
  actionButton: {
    backgroundColor: '#e1251a',
    borderRadius: 5,
    shadowColor: '#282828',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    paddingVertical: 15,
    paddingHorizontal: 40,
    marginTop: 15
  }
})

const mapStateToProps = state => {
  console.log(state.product)
  const {
    id, name, description
  } = state.product

  return {
    id, name, description
  }
}

export default connect(mapStateToProps, {})(Verification)
