
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import * as actions from '../store/actions/CartActions'


class Product extends Component {
  addToCart = () => {
    console.log("addToCart");
    Alert.alert(
      'Agregar a carrito',
      '¿Está seguro que desea agregar este producto al carrito?',
      [
        {
          text: 'Cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Agregar', onPress: () => {
          console.log(this.props.product);
          //this.props.addProductToCart(this.props.product);
          Alert.alert('Artículo agregado satisfactoriamente');
          this.props.resetProduct();
          Actions.reset('main');
        }},
      ],
      {cancelable: false},
    );
  }
  render () {
    const { name } = this.props.product;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          - Product - Dummy Screen
        </Text>
        <Text style={[styles.welcome, { fontSize: 24, fontWeight: 'bold' }]}>
          { name }
        </Text>
        <Text
          style={styles.welcome}
          onPress={() => Actions.reset('main')} 
        >
          Regresar a QR
        </Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => this.addToCart()} 
          style={styles.actionButton}
        >
          <Text style={{ color: '#fff', fontSize: 20 }}>Agregar a Carrito</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  actionButton: {
    backgroundColor: '#e1251a',
    borderRadius: 5,
    shadowColor: '#282828',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    paddingVertical: 15,
    paddingHorizontal: 40,
    marginTop: 15
  }
});

const mapStateToProps = state => {
  console.log(state);
  const {
    product
  } = state

  return {
    product
  }
}

export default connect(mapStateToProps, actions)(Product);
