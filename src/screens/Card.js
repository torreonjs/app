
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Button,
  Alert,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { CreditCardInput } from "react-native-credit-card-input";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      readyPay: false,
    }
  }

  _onChange = form => {
    this.setState({
      readyPay: form.valid
    });
  };

  renderButton = () => {
    if (this.state.readyPay) {
      return(
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => this._createOrder()}
          style={styles.actionButton}
        >
          <Text style={{ color: '#fff', fontSize: 20 }}>Pagar</Text>
        </TouchableOpacity>
      )
    }
    return(<View/>)
  };

  _createOrder = () => {
    console.log("createOrder")
    Alert.alert("Tu pago ha sido efectuado.  !Gracias por comprar en Elektra!");
    Actions.reset('main')
  };

  render () {
    const { name } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <CreditCardInput onChange={this._onChange} />
          <View style={{ alignItems: 'center' }}>
            { this.renderButton() }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    flexGrow: 1
  },
  
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    paddingTop: 30,
    alignItems: 'center'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  actionButton: {
    backgroundColor: '#e1251a',
    borderRadius: 5,
    shadowColor: '#282828',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    paddingVertical: 15,
    marginTop: 45,
    width: 150,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapStateToProps = state => {
  return {}
}

export default connect(mapStateToProps, {})(Card);
