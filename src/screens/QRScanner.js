import React, { Component } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Image,
  View
} from 'react-native'
import { connect } from 'react-redux'
import { Actions } from 'react-native-router-flux'
import QRCodeScanner from 'react-native-qrcode-scanner'
import * as actions from '../store/actions/ProductActions'
import cart from '../img/cart.png'

class QRScanner extends Component {
  onSuccess = (e) => {
    console.log('onSuccess')
    console.log(e.data)
    const { catalog } = this.props
    var product = {}
    Object.keys(catalog).forEach(function (item) {
      if (catalog[item].id == e.data) {
        product = catalog[item]
      }
    });
    this.props.setProduct(product)
    Actions.reset('product')
  }

  render() {
    return (
      <View>
        <TouchableOpacity 
          style={styles.cartButton}
          onPress={() => Actions.cart()} 
        >
          <Image
            source={cart}
            style={{ width: 25 }}
            resizeMode='contain'
          />
        </TouchableOpacity>
        <QRCodeScanner
          onRead={this.onSuccess}
          topContent={
            <Text style={styles.centerText}>
              QR code scanner.
            </Text>
          }
          // bottomContent={
          //   <Text style={styles.centerText}>
          //     QR code scanner.
          //   </Text>
          // }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
  cartButton: {
    width: 50,
    height: 50,
    backgroundColor: '#e1251a',
    borderRadius: 25,
    zIndex: 200,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 10,
    right: 10
  }
})

const mapStateToProps = state => {
  
  const {
    catalogList
  } = state.catalog

  return {
    catalog: catalogList
  }
}

export default connect(mapStateToProps, actions)(QRScanner);
