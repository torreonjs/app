
import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native'
import firebase from 'react-native-firebase'
import { Actions } from 'react-native-router-flux'
import { connect } from 'react-redux'
import * as actions from '../store/actions/AuthActions'
import logo from '../img/logo.png'

class Login extends Component {
  constructor (props) {
    super(props)

    this.state = {
      phone: ''
    }

    this.send = this.send.bind(this)
  }

  componentDidMount () {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log('login', user, user.toJSON())
        this.props.setUserProfile(user.toJSON())
        Actions.reset('main')
      } else {
        this.props.setUserProfile({})
      }
    })
  }

  componentWillUnmount () {
    if (this.unsubscribe) this.unsubscribe()
  }

  async send () {
    try {
      const confirmation = await firebase.auth().signInWithPhoneNumber(`+52${this.state.phone}`)
      if (confirmation) {
        console.log('user credentials ->', confirmation)
        Actions.verification({ confirmation })
      }
    } catch (e) {
      console.error(e)
    }
  }

  render () {
    const { phone } = this.state
    console.log('phone', phone.length)
    return (
      <View style={styles.container}>
        <Image
          source={logo}
          resizeMode='contain'
          cache='force-cache'
          style={{ width: 300 }}
        />
        <Text style={styles.welcome}>
          Ingresa tu celular
        </Text>
        <TextInput
          underlineColorAndroid={'transparent'}
          autoCorrect={false}
          selectionColor={'#fff'}
          placeholder={'(871) 223 4149'}
          keyboardType='phone-pad'
          maxLength={10}
          placeholderTextColor={'#999999'}
          onChangeText={(text) => { this.setState({ phone: text }) }}
          value={phone}
          returnKeyType={'next'}
          onSubmitEditing={this.send}
          blurOnSubmit={false}
          style={styles.fullInput}
        />
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={this.send}
          style={styles.actionButton}
        >
          <Text style={{ color: '#fff', fontSize: 20 }}>Continuar</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffdc20'
  },
  welcome: {
    fontSize: 24,
    textAlign: 'center',
    margin: 10,
    color: '#e81f25'
  },
  fullInput: {
    width: 300,
    backgroundColor: '#fff',
    padding: 15,
    borderRadius: 5,
    fontSize: 16
  },
  actionButton: {
    backgroundColor: '#e1251a',
    borderRadius: 5,
    shadowColor: '#282828',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    paddingVertical: 15,
    paddingHorizontal: 40,
    marginTop: 15
  }
})

const mapStateToProps = state => {
  const { user } = state
  return { user }
}

export default connect(mapStateToProps, actions)(Login)
