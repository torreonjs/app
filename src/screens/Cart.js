
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import SvgUri from 'react-native-svg-uri';

class Cart extends Component {
  renderCart = () => {
    const products = this.props.cart.products;
    return (
      <View style={styles.content}>
          { Object.keys(products).map((data) => {
            return (
              <View key={data} style={{ marginBottom: 10 }}>
                <Text style={{ fontSize: 22 }}>
                  {products[data].name}
                </Text>
              </View>
            )
          }) 
        }
      </View>
    )
  }

  render () {
    const { name } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.content}>
          {this.renderCart()}
        </ScrollView>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => Actions.card()} 
          style={styles.actionButton}
        >
          <Text style={{ color: '#fff', fontSize: 20, textAlign: 'center' }}>Continuar con la Compra</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
    backgroundColor: '#F5FCFF'
  },
  header: {
    flex: 1,
    backgroundColor: 'yellow'
  },
  content: {
    flex: 5,
  },
  footer: {
    flex: 1,
    backgroundColor: 'red'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  actionButton: {
    backgroundColor: '#e1251a',
    borderRadius: 5,
    shadowColor: '#282828',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 7,
    paddingVertical: 15,
    paddingHorizontal: 40,
    marginTop: 15,
    width: 350
  }
});

const mapStateToProps = state => {
  const {
    cart
  } = state

  return {
    cart
  }
}

export default connect(mapStateToProps, {})(Cart);
