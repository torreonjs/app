
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

class Orders extends Component {
  render () {
    const { name } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Product
        </Text>
        <Text style={styles.welcome}>
          Name: { name }
        </Text>
        <Text
          style={styles.welcome}
          onPress={() => Actions.qrscanner()} 
        >
          Go to QRScanner
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

const mapStateToProps = state => {
  console.log(state.product);
  const {
    id, name, description
  } = state.product

  return {
    id, name, description
  }
}

export default connect(mapStateToProps, {})(Orders);
