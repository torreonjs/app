import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native'
import { Router, Scene } from 'react-native-router-flux'
import logo from './img/logo.png'
import notifications from './img/tabs/notifications.png'
import profile from './img/tabs/profile.png'
import scan from './img/tabs/scan.png'
import orders from './img/tabs/orders.png'
import Landing from './screens/Landing'
import Login from './screens/Login'
import Verification from './screens/Verification'
import QRScanner from './screens/QRScanner'
import Cart from './screens/Cart'
import Product from './screens/Product'
import Card from './screens/Card'

class AppRouter extends Component {
  render () {
    const headerLogo = () => {
      return (
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingLeft: 10
        }}>
          <Image
            source={logo}
            style={{ width: 100 }}
            resizeMode='contain'
          />
        </View>
      )
    }
    const HomeIcon = ({ selected, title }) => (
      <Image
        source={scan}
        style={{ width: 25 }}
        resizeMode='contain'
      />
    )

    const OrdersIcon = ({ selected, title }) => (
      <Image
        source={orders}
        style={{ width: 25 }}
        resizeMode='contain'
      />
    )

    const NotificationsIcon = ({ selected, title }) => (
      <Image
        source={notifications}
        style={{ width: 25 }}
        resizeMode='contain'
      />
    )

    const ProfileIcon = ({ selected, title }) => (
      <Image
        source={profile}
        style={{ width: 25 }}
        resizeMode='contain'
      />
    )

    return (
      <Router navigationBarStyle={styles.header} renderTitle={headerLogo}>
        <Scene key='root'>
          <Scene
            key='login'
            component={Login}
            title='Login'
            hideNavBar
          />
          <Scene
            key='verification'
            component={Verification}
            title='Verification'
            hideNavBar
          />
          <Scene
            key='main'
            labelStyle={{ color: 'red' }}
            tabs
            hideNavBar
            hideBackImage
          >
            <Scene
              key='qrscanner'
              component={QRScanner}
              title='QR'
              icon={HomeIcon}
              initial
            />
            <Scene key='landing'
              component={Landing}
              title='Mis Ordenes'
              icon={OrdersIcon}
              hideNavBar
            />
            <Scene key='notifications'
              component={Landing}
              title='Notificaciones'
              icon={NotificationsIcon}
              hideNavBar
            />
            <Scene key='profile'
              component={Landing}
              title='Mi Cuenta'
              icon={ProfileIcon}
              hideNavBar
            />
          </Scene>
          <Scene
            key='card'
            component={Card}
            title='Card'
            hideNavBar
          />
          <Scene
            key='product'
            component={Product}
            title='Product'
            hideNavBar
          />
          <Scene
            key='cart'
            component={Cart}
            title='Cart'
            hideBackImage
          />
        </Scene>
      </Router>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#ffdc20',
    flex: 1
  }
})

export default AppRouter
